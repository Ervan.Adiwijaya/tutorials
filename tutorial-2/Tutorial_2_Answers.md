# Jawaban Tutorial 2 Game Development

> Nama : Ervan Adiwijaya Haryadi
>
> NPM  : 1706074871

1.  Fungsi node bertipe `Sprite` adalah node yang menampilkan tekstur 2 dimensi pada scene.

2.  `RigidBody2D` adalah objek node 2 dimensi yang terpengaruh oleh *physics* (contohnya gravitasi) yang berlaku di dalam game. Sedangkan `StaticBody2D` adalah objek node 2 dimensi yang **seharusnya** tidak bisa digerakkan (kecuali memang disengaja) karena tidak terpengaruh secara langsung dengan *physics* yang berlaku di dalam game. `StaticBody2D` umumnya digunakan sebagai pembentuk tembok atau platform.

3.  Tidak ada pengaruh antara `Mass` & `Weight` terhadap kecepatan waktu jatuh objek sehingga tidak terlihat ada perbedaan saat game dijalankan. Jika yang ingin dicapai adalah mengubah kecepatan jatuh objek akibat gravitasi, yang diubah adalah atribut `Gravity Scale` pada objek tersebut.

4.  Saat atribut `Disabled` dinyalakan pada scene `StonePlatform` dan scene `Main` dijalankan, pesawat akan jatuh "menembus" platform yang ada dibawahnya. Hal ini terjadi karena platform tersebut sebenarnya tidak nyata ada di *environment* game tersebut sehingga tidak bisa dipijak oleh pesawat. Percobaan lebih dalam meyakinkan saya bahwa atribut `Disabled` ini dapat digunakan untuk membuat gambar latar belakang *environment* game.

5.  Mengubah atribut `Position` pesawat akan mengubah posisi pesawat berdasarkan *main axis* X dan Y, `Rotation` akan mengubah arah hidung pesawat mengarah (merotasi objek/scene), dan `Scale` akan mengubah ukuran pesawat menjadi lebih besar atau kecil (pembesaran berdasarkan X axis dan Y axis dari objek tersebut).

6.  Karena `StonePlatform` dan `StonePlatform2` adalah node anak (**child node**) dari node `PlatformBlue`. Transformasi node `PlatformBlue` mengacu
pada XY axis yang dimiliki node Main, sedangkan transformasi posisi node `StonePlatform` dan `StonePlatform2` mengacu kepada posisi `PlatformBlue`
yang telah didefinisikan sebelumnya.

### Latihan ekstra sudah terupload