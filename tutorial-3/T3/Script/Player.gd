extends KinematicBody2D

onready var sprite = get_node("Sprite")

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)

var velocity = Vector2()
var able_to_dbl_jmp = false
var anim = "idle"

func get_input():
	velocity.x = 0	# deklarasi private variabel tipe Vector2
	
	# perubahan kecepatan
	if able_to_dbl_jmp == true and Input.is_action_just_pressed('up'):
		velocity.y = 0
		velocity.y += jump_speed
		able_to_dbl_jmp = false
		
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y += jump_speed
		able_to_dbl_jmp = true
	
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	
	if is_on_floor() :
		if velocity.x == 0 :
			anim = "idle"
		elif velocity.x != 0 :
			anim = "running"
			if velocity.x > 0 :
				sprite.set_flip_h(false)
			elif velocity.x < 0 :
				sprite.set_flip_h(true)
	else :
		if velocity.y < 0 :
			anim = "jump"
		elif velocity.y > 0 :
			anim = "fall"
			
		if Input.is_action_pressed("right"):
			sprite.set_flip_h(false)
		if Input.is_action_pressed("left"):
			sprite.set_flip_h(true)
	sprite.play(anim)
	
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
